<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('movimientos', function() {
    return "Esta es la página de movimientos.";
});

Route::get('about', function() {
    return "Sofia, web contable. Manuel Motta 2020. Todos los derechos reservados.";
});

Route::get('perfil/{cliente?}', function($cliente = "Invitado") {
    return "Este es el perfil de " . $cliente;
});